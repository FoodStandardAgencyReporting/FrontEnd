**Requirements:**
1. npm
2. angular-cli
2. port 420 open

**To Run:**
1. npm install
2. npm start

**Description:**

AngularJs (2) app that utilizes NodeJs back end project. Simple interface - drop down list and confirmation button with report section below. 


**How this thing works:**

Main component makes initial call to get all local authorities. This data is used to populate the select control. 
When user clicks on "Get Report" button, another request is made to get detail data.

**Settings:**

BackEndUrl = 'http://ec2-52-201-235-223.compute-1.amazonaws.com:8081';

**Known issues:**

Tech debt: No tests, no screen automation. 
