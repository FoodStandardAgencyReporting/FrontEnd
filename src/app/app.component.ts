
"use strict"

import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { BackEndUrl } from './config/config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  public authorieties: any;
  public selectedAuthority: any;
  public scores: any;
  public ObjectKeys = Object.keys;

  constructor(private http: HttpClient) {
    this.http.get(BackEndUrl + '/authority/get-all').subscribe(data => {

      this.authorieties = data;
    });
  }

  public getData() {
    if (this.selectedAuthority == undefined) {
      alert("Please select authority from the list first...");
      this.scores = null;
      return;
    }

    this.http.get(BackEndUrl + '/authority/' + this.selectedAuthority + '/report').subscribe(data => {
      this.scores = data;
    });
  }
}